﻿/*
* This code provides a proof of concept of vulnerability flaws found during a security analysis of the Arduino Yun device.
*
*
* More information can be found in the paper: 
*     "Security Analysis and Exploitation of Arduino devices in the Internet of Things"
*   Presented at "1st International Workshop for Malicious Software and Hardware in Internet of Things (MAL-IoT 2016)", in Como, Italy, May 16, 2016 
*
*  Authors:
*     Carlos Alberca, Sergio Pastrana, Guillermo Suarez de Tangil, Paolo Palmieri
*
*/

#include <SoftwareSerial.h>         //Software Serial Port, it is to use another serialport and can use default Serial to debug.
#include <Process.h>

#define LED 13

#define RxD 10                      // This is the pin that the Bluetooth (BT_TX) will transmit to the Arduino (RxD)
#define TxD 11                      // This is the pin that the Bluetooth (BT_RX) will receive from the Arduino (TxD)
#define CONFIG_BT 0                 // 0 -> no configure bluetooth ; 1 -> configure bluetooh
#define DELAY_CONFIG_BT 1500

SoftwareSerial BTSerial(RxD, TxD);  // Setup the Arduino to receive INPUT from the bluetooth shield on Digital Pin RxD ; // Setup the Arduino to send data (OUTPUT) to the bluetooth shield on Digital Pin TxD

// *** Bluetooth Variables ***
char ssid[15] = "ArduinoYunBT";     // BT name device. Max 20 char
char pass[5] = "1234";              // KEY to pair device. Max 4 char
char baud_rate = '4';               // Bits/second: 1=1200, 2=2400, 3=4800, 4=9600 (Por defecto), 5=19200, 6=38400, 7=57600, 8=115200, 9=230400, A=460800, B=921600, C=1382400


/* ********************* SETUP ********************* */
void setup() {
  Serial.begin(9600);    // opens serial port, sets data rate to 9600 bps
  while (!Serial);
  
  Serial.println(F("\nStarting Bridge..."));
  Bridge.begin();

  pinMode(LED, OUTPUT);   // Use onboard LED (if required)

    // Config Bluetooth
    BTSerial.begin(9600); //Set BluetoothBee BaudRate to default baud rate 9600
    delay(1000);
    if (CONFIG_BT == 1) {
        setup_BT();  // Start BT configuration
    } else {
        Serial.println(F("Setup old configuration in BT device -> OK "));
    }
  
  Serial.println(F("Setup OK...\n"));
}


/* ********************* LOOP - WHERE 'HEAP OVERFLOW' MAY BE EXPLOITED ********************* */
void loop() {

  Process cmd;
  String btData = "";
  char *argument, *command;
  
  //These variables are allocated in the Heap.
  argument = (char *) malloc(10*sizeof(char));
  command = (char *) malloc(20*sizeof(char));
  
  // The original command which can be overwritten
  strcpy(command, "ifconfig ");
  
  // To show the correctness of the overflow
  // print the original of the command
  Serial.print(F("\n\nCommand variable before Heap Buffer Overflow: \n"));
  Serial.print(command);
  BTSerial.print(F("\n\nCommand variable before Heap Buffer Overflow: \n"));
  BTSerial.print(command);

  // Asking for a parameter. 
  // Payload example performing a Heap Buffer Overflow: "eth1 aaaaa cat /etc/shadow".
  Serial.print(F("\n\nWhat interface do you want to up or down or get the info?\n"));
  BTSerial.print(F("\n\nWhat interface do you want to up or down or get the info?\n"));
  while (!BTSerial.available());

  // Vulnerability-> The program do not check bounds!
  while (BTSerial.available()) {
    //gets one byte from serial buffer
    char c = BTSerial.read();
    delay(50);
    btData.concat(c);
  }

  // Security check: Checks if the user wants to do more than one command
  if(btData.indexOf('&') == -1 && btData.indexOf('`') == -1){
    // '&' and '`' characteres are not found

    // HEAP OVERFLOW
    strcpy(argument, btData.c_str());

    // To show the correctness of the overflow
    // print the modified value of the command
    Serial.print(F("\n\nCommand variable after Heap overflow: \n"));
    Serial.write(command);
    BTSerial.print(F("\n\nCommand despues after Heap overflow: \n"));
    BTSerial.write(command);
    
    //execute the command
    int exitCode = cmd.runShellCommand(command + (String)argument); 
    showOutputProcess(cmd);
  }else{
    Serial.print(F("\n\nError: Security check failed. Command is incorrect.\n"));
    BTSerial.print(F("\n\nError: Security check failed. Command is incorrect.\n"));
    return;
  }

  delay(5000); // 5 seconds
}


/* ********************* PRINTS THE OUTPUT OF THE COMMAND FROM THE OPENWRT CONSOLE ********************* */
void showOutputProcess(Process p) {
  while (p.running());

  Serial.println();
  BTSerial.println();
  while (p.available() > 0) {
    char c = p.read();
    Serial.print(c);
    BTSerial.print(c);
  }
  Serial.flush();
  BTSerial.flush();
}


/* ********************* SETUP BLUETOOTH ********************* */
//The following code is necessary to setup the bluetooth shield
void setup_BT()
{
    digitalWrite(LED, HIGH); // Waiting BT connection
    Serial.println("Starting setup BT...");
    delay(5000);
    digitalWrite(LED, LOW);     // Start configuration
    
    BTSerial.print("AT"); delay(DELAY_CONFIG_BT);// This delay is required. Delay required is 1000, but we use this one for security.
    // Set BT Name
    BTSerial.print("AT+NAME"); BTSerial.print(ssid); delay(DELAY_CONFIG_BT);
    // Set BaudRate
    BTSerial.print("AT+BAUD"); BTSerial.print(baud_rate); delay(DELAY_CONFIG_BT);
    // Set Pass:
    BTSerial.print("AT+PIN"); BTSerial.print(pass); delay(DELAY_CONFIG_BT);
    // Get Version:
    //bluetooth.print("AT+VERSION"); delay(1500);
    BTSerial.flush(); // Clean serial port memory
    
    // Flicker LED to notify BT is ready
    for (int i = 0; i < 10; i++) {
        digitalWrite(LED, !digitalRead(LED));
        delay(200);
    }
    digitalWrite(LED, HIGH);
    Serial.println("Setup new configuration BT device -> OK...");
}

\section{Casi di Studio}

\subsection{Cinematica} 
L'analisi degli intervalli può essere applicata nella risoluzione di sistemi di equazioni derivati da problemi di cinematica inversa o diretta. Consideriamo un problema di cinematica diretta di un robot parallelo.

\begin{figure}[H]
\begin{center}
\includegraphics[width=0.5\textwidth]{\imgspath Parallel_Robot}
\caption{Schema di un robot parallelo}
\end{center}
\end{figure}

Un primo passo è determinare l'insieme di equazioni più appropriate che modellino il sistema, dopodichè, fissata la lunghezza delle gambe, vanno stabilite quali possono essere le pose della piattaforma.\\
Assumiamo che la geometria della piattaforma e la sua posa sia definita da tre coordinate che rappresentano i punti di ancoraggio $B_{1}, B_{2}, B_{3}$, da queste informazioni possiamo derivare $B_{4}, B_{5}, B_{6}$ come:
\begin{gather*}
OB_{i} = \lambda_{i}OB_{1} + \alpha_{i}OB_{2} + \beta_{i}OB_{3}
\end{gather*}

in cui $\lambda_{i},\alpha_{i}, \beta_{i}$ sono costanti note. Deriviamo poi 6 vincoli esprimibili medianti equazioni del tipo:
\begin{gather*}
||A_{i}B_{i}||^{2} = \rho_{i}^{2}\ , \ i \in [1, 6]
\end{gather*}

e tre vincoli addizionali:
\begin{gather*}
||B_{i}B_{j}||^{2} = d_{ij}^{2}\ , \ (i, j) \in [1, 3] \wedge i \neq j.
\end{gather*}

dove $p_{ij}$ è la lunghezza della gamba $ij$ e $d_{ij}$ una distanza nota. Questo insieme di 9 equazioni in 9 incognite ha il vantaggio che le variabili appaiono solo una volta in ogni equazione e conseguentemente non ci sarà sovrastima durante la valutazione dell'intervallo, inoltre la geometria del problema impone dei limiti alle variabili.

L'algoritmo che risolve le equazioni fa utilizzo dei metodi di filtraggio 2B, 3B, metodo di Newton, operatore esistenziale basato sul teorema di Kantorovich e Neumaier test, la procedura completa viene descritta in\cite{wiki:kantTheorem} e fornisce le soluzioni esatte, nel senso che non avviene alcuna perdita e una soluzione è calcolata con un grado di precisione arbitrario.

In termini di tempi di computazione, l'algoritmo trova tutte le soluzioni in un intervallo temporale che va da qualche secondo a un minuto, ciò lo rende il secondo metodo di risoluzione più efficiente. Il migliore: Groebner richiede un tempo che varia da 1 a 30 secondi, ma a differenza di quest'ultimo, il tempo di computazione decresce con la taglia dello spazio di ricerca.

\subsection{Analisi delle Singolarità}

È un problema importante specialmente nei sistemi di controllo ad anello chiuso, per stabilire gli stati in cui il robot diventa incontrollabile. Se $J^{-1}$ denota la matrice Jacobiana nella risoluzione analitica di problemi di cinematica inversa, una singolarità viene definita come lo stato in cui $|J^{-1}| = 0$.

Dal punto di vista pratico il problema consiste nel rilevare se un componente robotico nella traiettoria o workspace in cui lavora incontra una singolarità e ciò è di critica importanza per la sicurezza dei robot.

Trovare le singolarità è un processo complesso, in più va considerato il problema dell'incertezza nella posizione $A_{i}, B_{i}$ (le coordinate dei punti di ancoraggio). Per risolvere queste problema si utilizza il concetto di continuità del determinante rispetto a $X$ (pose del workspace).

Assumiamo che per alcune pose $X_1$ del workspace siamo in grado di dimostrare che $|J^{-1}|$ abbia segno costante (positivo), se si è in grado di dimostrare che qualche altra posa $X_2$ ha il determinante negativo, allora ogni percorso che connette $X_1$ a $X_2$ deve attraversare una singolarità e conseguentemente il workspace è singolare.

Dunque determinare la posa $X_2$ è una proprietà che può essere studiata con l'analisi degli intervalli. Per iniziare assumiamo non ci sia incertezza nel modello  del robot, assumiamo inoltre che $X$ appartenga ad un box: gli algoritmi classici per il calcolo del determinante di una matrice (es. eleminazione di Gauss) possono essere estesi ad una versione intervallare, in tal caso è possibile ottenere una valutazione per ogni elemento di $|J^{-1}|$ per il box e lo Jacobiano diventa una matrice di intervalli.

Scelta in modo arbitrario una posa $X_{t}$ dal workspace, viene calcolata la sua valutazione intervallare, se la valutazione non consente di mostrare che il determinante ha segno costante (il limite inferiore e superiore della valutazione non hanno lo stesso segno), allora il workspace non è libero da singolarità.

Assumiamo ora che la posa $X_{1}$ sia stata trovata, avendo la possibilità di calcolare una valutazione intervallare del determinante è sufficiente a modellare un algoritmo $A$, inoltre l'utilizzo di filtri riducono drasticamente i tempi di calcolo. Dato un box $B$, prima calcoliamo la valutazione intervallare del determinante $U$ della matrice $KJ^{-1}(B)$, con $K$ matrice di scalari scelta come inversa di $J^{-1}$ calcolata per il mid-point di $B$ (se esiste).
Sia $U = |KJ^{-1}| = |K||J^{-1}|$, se $|K|$ ha segno costante, allora possiamo dedurre che $|J^{-1}(B)|$ abbia segno costante. Se il segno è negativo, allora il workspace include una singolarità (tutte le pose in $B$ sono pose $X_{2}$), altrimenti $B$ non include $X_{2}$ e può essere scartato.

Notiamo che per ridurre la sovrastima di $U$ è necessario calcolare una forma chiusa di $KJ^{-1}$ e riarrangiare gli elementi di questa matrice per ridurre il numero di occorrenze multiple degli elementi di $X$.

Un secondo filtro coinvolge un teorema fornito da Rohn\cite{jansson1999algorithm}.

Consideriamo l'insieme di matrici estremali $Z$, derivato dalla matrice a intervalli $J^{-1}(B)$, cioè tutte le matrici di scalari i cui elementi o sono un limite inferiore o un limite superiore della corrispondente matrice a intervalli $J^{-1}(B)$.

Se tutte le matrici $Z$ hanno il determinante con lo stesso segno, allora non c'è una matrice singolare in $J^{-1}(B)$, ovvero se il determinante di $J^{-1}$ di un'arbitraria posa inclusa nel box $B$ è positivo, allora $B$ non include pose $X_{2}$ e può essere scartato.

Da notare che per una matrice $J^{-1}$, $N \times N$, l'insieme $Z$ ha $2^{n^{2}}$ matrici, ma Rohn ha provato che è sufficiente un sottoinsieme di $Z$ che ha $2^{2n - 1}$ elementi.

Quindi l'algoritmo $A$ utilizzando entrambi i filtri è molto efficiente: controlla la regolarità di $J^{-1}$ su un ampio workspace richiedendo meno di un secondo\cite{merlet2006regularity}.

Se consideriamo l'incertezza in fase di modellazione dei robot la struttura dell'algoritmo $A$ non cambia. Se l'intervallo che descrive queste incertezze ha una larghezza ridotta, le terremo semplicemente in considerazione per calcolare la valutazione a intervalli degli elementi di $J^{-1}$. Tuttavia può capitare che data una posa, non siamo in grado di determinare il segno di $J^{-1}$ a causa di queste incertezze, in tal caso vengono aggiunte come incognite nell'algoritmo $A$, comportando un significativo aumento dei tempi calcolo (potrebbe richiedere alcune ore), ma consentendo di ottenere il più ampio livello di sicurezza: se l'algoritmo prova che il workspace è libero da singolarità, allora lo è per il robot reale.




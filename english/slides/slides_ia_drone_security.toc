\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\beamer@endinputifotherversion {3.36pt}
\defcounter {refsection}{0}\relax 
\select@language {italian}
\defcounter {refsection}{0}\relax 
\select@language {italian}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {1}{Analisi degli Intervalli}{3}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {1}{1}{Introduzione}{5}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {1}{2}{Valutazione degli Intervalli}{8}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {1}{3}{Esempio}{11}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {2}{Casi di Studio}{20}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{1}{Cinematica}{21}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{2}{Analisi delle Singolarit\IeC {\`a}}{25}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {3}{Fault Tree e Analisi degli Intervalli}{32}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {4}{Arduino: possibili vulnerabilit\IeC {\`a}}{52}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{1}{Specifiche}{53}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{2}{Analisi di sicurezza}{60}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {5}{Analisi del nostro Firmware Arduino}{66}{0}{5}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {5}{1}{Panoramica}{67}{0}{5}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {5}{2}{Dichiarazioni di variabili}{70}{0}{5}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {5}{3}{Controllo sulla dimensione degli indici}{74}{0}{5}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {5}{4}{Controllo di intervalli di valori}{79}{0}{5}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {6}{Bibliografia}{83}{0}{6}

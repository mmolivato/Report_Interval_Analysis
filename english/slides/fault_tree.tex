\section{Fault Tree e Analisi degli Intervalli}

\begin{frame}
\frametitle{Introduzione}
Fault Tree \cite{dugan1996new}, \cite{vesely1981fault}, \cite{visinsky1994robotic} è un tool per l'analisi dell'affidabilità molto utilizzato dalle industrie nucleari e aerospaziali, dove il concetto di affidabilità può essere definito in diversi modi:
\begin{itemize}
\item L'idea che un oggetto o un dispositivo sia idoneo per l'impiego nel tempo.
\item La capacità di un dispositivo o di un sistema di svolgere in modo appropriato la funzione per la quale è stato progettato.
\item La caratteristica di un dispositivo o sistema di funzionare senza guastarsi.
\item La capacità di un dispositivo o di un sistema di eseguire la funzione prevista in condizioni specificate per un determinato periodo di tempo.
\item  La probabilità che un dispositivo o di un sistema svolga la funzione prevista nelle condizioni specificate per un determinato periodo di tempo.
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Introduzione}
Nel caso della robotica i Fault Tree associati all'Analisi degli Intervalli è diventato uno degli approcci che consente di analizzare l'affidabilità dei robot e trattare problemi di manipolazione di dati incerti ed errori di approssimazione numerica.
Questo approccio codifica il set di dati in termini di intervalli, propagati poi nel Fault Tree per generare un output distribuito che riflette l'incertezza dei dati in input.
\end{frame}

\begin{frame}
\frametitle{Caso di Studio}
Come esempio consideriamo un braccio robotico con tre articolazioni (joint) che opera in un piano.
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.80\textwidth]{\imgspath Robotic_arm}
\caption{Schema di un braccio robotico composto da giunti, attuatori e sensori}
\end{center}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Caso di Studio}
Il braccio possiede un attuatore e due sensori ridondanti (due sensori di posizione o un sensore di posizione e un sensore di velocità) ad ogni joint, che consentono di avere un certo grado di tolleranza al fallimento.
Dallo scenario appena descritto si può ricavare il seguente Fault Tree:
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.90\textwidth]{\imgspath Fault_tree}
\caption{Rappresentazione tramite porte logiche di un Fault Tree}
\end{center}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Caso di Studio}
\textbf{Notazioni:}
\begin{itemize}
\item $Ji$ indica l'i-esimo joint;
\item $Mi$ indica l'i-esimo attuatore;
\item $SiA$ e $SiB$, il sensore primario (A) e di backup (B) del joint i-esimo;
\item $u, v, w, x$ Interval points della probabilità di guasto di un componente. 
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Caso di Studio}
Per il Fault Tree rappresentato in figura, un algoritmo per calcolare la probabilità di guasto alla radice dell'albero ($s$), dagli inputs ($a, b, c, d, e, f, g, h, i$) è dato da:
\begin{itemize}
\item $j = a$ x $b$
\item $k = c$ x $d$
\item $l = e$ x $f$
\item $m = g + j$
\item $n = h + k$
\item $o = i + l$
\item $p = m$ x $n$
\item $q = m$ x $o$
\item $r = n$ x $o$
\item $t = p + q$
\item $s = r + t$
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Caso di Studio}
Dove "$a$","$b$" sono le probabilità di guasto del sensore primario e ridondante del joint 1 e "$c$","$d$" ed "$e$","$f$" sono le probabilità di guasto dei sensori primari e ridondanti dei joint 2 e 3. Rispettivamente "$g$", "$h$, "$i$" sono le probabilità di guasto associate agli attuatori dei joints 1, 2 e 3. Quindi un evento di guasto al braccio robotico (fallimento che si propaga fino alla radice) si verifica se almeno due joint falliscono.

L'affidabilità $R$ di un robot è quindi espressa dall'equazione:
\begin{gather*}
R = 1 - s
\end{gather*}
\end{frame}

\begin{frame}
\frametitle{Caso di Studio}
I dati di input sono espressi in termini di intervalli estesi con una "massa", potendo così rappresentare una "densità" nella forma di istogrammi.
Dagli istogrammi viene calcolata la densità di probabilità di un evento di interesse, come il fallimento del sistema (nodo radice).

Questa rappresentazione fornisce una simulazione esaustiva dello spazio di input in tempo ragionevole, inoltre vi è la garanzia che i risultati ottenuti dalla simulazione del caso pessimo, vengano inclusi negli istogrammi in output.
\end{frame}

\begin{frame}
\frametitle{Caso di Studio}
La rappresentazione mediante istogrammi fa utilizzo di alcuni modelli predefiniti chiamati "\emph{griglie}" (grids), che consentono di stabilire la granularità dei dati e controllare il tradeoff tra l'accuratezza del risultato e la complessità di calcolo.
\end{frame}

\begin{frame}
\frametitle{Caso di Studio}
Le griglie si possono definire come una lista di intervalli disgiunti e adiacenti che coprono l'intero intervallo numerico di interesse.
Si possono utilizzare griglie sia per controllare la taglia dello spazio di input, sia per il processo di raccolta dei dati e sono caratterizzate da quattro parametri:
\begin{itemize}
\item il \textbf{tipo}: lineare (formato da intervalli di taglia uguale) o geometrico (formato da intervalli la cui taglia cresce esponenzialmente a partire dal centro di simmetria);
\item la \textbf{granularità} ($g$): rappresenta la taglia dei suoi intervalli;
\item il \textbf{centro di simmetria} ($c$): la griglia sarà simmetrica rispetto al suo centro di simmetria;
\item il \textbf{focus} ($f$): per ridurre la complessità di calcolo limitando la regione di spazio numerico che è analizzata in dettaglio.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Caso di Studio}
\begin{mydef}[\textbf{Griglia lineare}]
E' un insieme di intervalli adiacenti $[A, B]$, ognuno dei quali unicamente identificato da un intero $n$, che verifica una delle seguenti identità:

\begin{gather*}
[A, B] = \left\{
\begin{aligned}
& [-Inf, c + g(-f + 1)] & (n < -f) \\
& [c + gn + 1, c + g(n + 1)] & (-f < n < -1) \\
& [c - g + 1, c - 1] & (n = -1) \\
& [c, c] & (n = 0) \\
& [c + 1, c + g - 1] & (n = 1)  \\
& [c + g(n - 1), c + gn - 1] & (f > n > 1) \\
& [c + g(f - 1), Inf] & (n \geq f) \\
\end{aligned}
\right.
\end{gather*}

\end{mydef}
\end{frame}

\begin{frame}
\frametitle{Caso di Studio}
\begin{mydef}[\textbf{Griglia geometrica}]
E' un insieme di intervalli adiacenti $[A, B]$, ognuno dei quali unicamente identificato da un intero $n$, che verifica una delle seguenti identità:

\begin{gather*}
[A, B] = \left\{
\begin{aligned}
& [-Inf, c - g^{f - 1}] & (n < -f) \\
& [c - g^{-n} + 1, c - g^{-(n + 1)}] & (-f < n < 0) \\
& [c, c] & (n = 0) \\
& [c + g^{n - 1} + 1, c + g^{n} - 1] & (f > n > 0) \\
& [c + g^{f - 1}, Inf] & (n \geq f) \\
\end{aligned}
\right.
\end{gather*}

Con g, f interi positivi.
\end{mydef}
\end{frame}

\begin{frame}
\frametitle{Caso di Studio}
Gli istogrammi sono descritti in termini di intervalli che sono definiti come

\begin{mydef}
Un intervallo $[a, b]/p$ è un insieme $N = (b - a + 1)$ di interi $x$ che verifica $a \leq x \leq b$ con una massa di probabilità $p$.
$p$ è uniformemente distribuito in $[a, b]$ quindi la probabilità per ogni $x \in [a, b]$ può essere calcolata come $p/N$.
\end{mydef}
\end{frame}

\begin{frame}
\frametitle{Caso di Studio}
La definizione di intervallo fornita è basata sui numeri interi, quindi gli intervalli vengono preprocessati (postprocessati) per poter essere applicati in casi che utilizzano i numeri razionali.

In particolare, l'intervallo $[0, 1]$ viene partizionato in $M$ passi di discretizzazione di uguale misura rappresentati da un numero (\emph{midpoint}) e una probabilità (ottenuta dalla distribuzione originale). Così l'intervallo viene scalato nell'intervallo $[0, M - 1]$ e mappato in una distribuzione di interi nello stesso intervallo, dove ogni intero $n$ ha la probabilità del sottointervallo $[n - 0.5, n + 0.5]$ nella distribuzione scalata.
In generale, $M$ è selezionato in accordo con la precisione fornita nei dati in input, per esempio, se un input possiede $m$ cifre, allora $M=10^{m}$ e assume un ruolo fondamentale in quanto stabilisce il grado di precisione numerica.
\end{frame}

\begin{frame}
\frametitle{Caso di Studio}
Una volta che la distribuzione in input è stata rappresentata in termini di una griglia predefinita, il metodo di computazione esegue:

\begin{itemize}

\item Considerato lo spazio di input $N1 \times N2 \times ... \times NI$, dove $Ni$ è l'insieme di intervalli che descrive l'istogramma dell'input $i$;

\item Per ogni vettore $(..., [a_{ij}, b_{ij}]/p_{ij}, ...)$ dello spazio di input, dove $[a_{ij}, b_{ij}]/p_{ij}$ rappresenta la j-esima barra dell'istogramma che descrive l'input $i$:
\end{itemize}

\begin{itemize}

\item Calcola la sua probabilità $ P = \prod_{i = 1}^{I}p_{ij} $;
\item Esegue le operazioni usando gli intervalli aritmetici;
\item Assegna $P$ ad ogni intervallo risultante;
\item Raccoglie i risultati in istogrammi di output, applicando le regole di split e merge.

\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Caso di Studio}
\begin{mydef}[\textbf{Regola di split}] Ogni intervallo che risulta estendersi su diversi intervalli della griglia, viene partizionato in altrettanti intervalli con probabilità proporzionali.
\end{mydef}

\begin{mydef}[\textbf{Regola di merge}] Tutti i risultati dell'intervallo che risultano essere nello stesso intervallo della griglia, vengono rappresentati come un singolo intervallo con probabilità sommate.
\end{mydef}
\end{frame}

\begin{frame}
\frametitle{Caso di Studio}
Prendiamo in esempio il caso di un attuatore (motore elettrico), una distribuzione di input con gli istogrammi è costruita come segue:
\begin{itemize}
\item $m = (0, 0.000739) : 0.05$
\item $m = (0.000739, 0.00203) : 0.11$
\item $m = (0.00203, 0.00924) : 0.34$
\item $m = (0.00924, 0.04158) : 0.34$
\item $m = (0.04158, 0.10903) : 0.11$
\item $m = (0.10903, 1) : 0.05$
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Caso di Studio}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.6\textwidth]{\imgspath Istogramma_1}
\caption{Probabilità di fallimento di un motore}
\end{center}
\end{figure}
Così la funzione $m$, rappresentante la probabilità di fallimento di un attuatore $Mi$, ha lo 0.05 della sua massa di probabilità tra lo 0 e 0.000739, ecc.
\end{frame}

\begin{frame}
\frametitle{Caso di Studio}
Dal momento che l'approccio a intervalli calcola il Fault Tree per ogni possibile combinazione di intervalli di input, produce una stima completa della distribuzione in uscita, non aggiungendo complessità di calcolo significativa. 

Overhead computazionale $\simeq$ Overhead generazione random dei sample metodo Monte Carlo.

L'utilizzo dell'approccio a intervalli è particolarmente interessante per calcolare grandi spazi di input dominati da un grande numero di intervalli di input.
\end{frame}


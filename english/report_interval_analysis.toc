\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {italian}
\defcounter {refsection}{0}\relax 
\select@language {italian}
\contentsline {section}{\numberline {1}Obiettivi}{2}{section.1}
\contentsline {section}{\numberline {2}Analisi di Intervalli}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Intervalli Aritmetici}{2}{subsection.2.1}
\contentsline {section}{\numberline {3}Casi di Studio}{5}{section.3}
\contentsline {subsection}{\numberline {3.1}Cinematica}{5}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Analisi delle Singolarit\IeC {\`a}}{6}{subsection.3.2}
\contentsline {section}{\numberline {4}Fault Trees}{7}{section.4}
\contentsline {subsection}{\numberline {4.1}Definizione e regole}{11}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Algoritmo di computazione}{12}{subsection.4.2}
\contentsline {section}{\numberline {5}Possibili vulnerabilit\IeC {\`a} nei firmware Arduino}{13}{section.5}
\contentsline {subsection}{\numberline {5.1}L'ambiente Arduino}{13}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Analisi di Sicurezza sull'ambiente}{14}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Esempi di attacco}{16}{subsection.5.3}
\contentsline {section}{\numberline {6}Analisi del nostro firmware Arduino}{18}{section.6}
\contentsline {subsection}{\numberline {6.1}Dichiarazioni di variabili}{18}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Controllo sulla dimensione degli indici}{19}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Controllo di intervalli di valori}{20}{subsection.6.3}
\contentsline {section}{\numberline {7}Conclusioni}{21}{section.7}

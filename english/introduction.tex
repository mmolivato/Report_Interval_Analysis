\section{Interval Analisys}

The Intervals Analysis \cite{moore1966interval} is a powerful numerical method that solves a wide range of robotics problems, such as: robot reliability, localization, navigation, motion planning, collision detection and calibration .
Furthermore it is applied to problems of numerical approximation, modeling and robotic control.
In the worst case the computational complexity is exponential and the writing of an efficient algorithm requires a high level of experience and skill.

\subsection{Arithmetic Intervals}

The purpose of arithmetic intervals is to determine the maximum $ \overline{F} $ and the minimum $ \underline{F} $ of a $ \phi $ function.
This operation is called \textit{evaluation of the interval} of a function and leads us to the interval $ [\underline{F}, \overline{F}] $ which varies according to the unknowns.

If \textbf{X} denotes a set of intervals for the unknowns and $ X\ped{0} $ an instance of $ \textbf{X} $, then we have:
\begin{gather*}
\underline{F} \leq \phi(X_0) \leq \overline{F}
\end{gather*}
 
An evaluation of an interval can be calculated in different ways, the simplest one, called \textit {natural evaluation}, consists in the use of an interval version of all the mathematical operators.

For example, adding two intervals $ \textbf {a} = [\underline{a}, \overline{a}], \textbf{b} = [\underline{b}, \overline{b}] $ It is defined as
$ \textbf{a} + \textbf{b} = [\underline{a} + \underline{b}, \overline{a} + \overline{b}] $.
The evaluation of the arithmetic intervals of a function can be illustrated with the following example: suppose we want to evaluate the function $ \phi = x^{2} - 2x $ in the range $ [3,5] $.

In this case we can say that $ \forall x \in [3, 5] $, then $ x^{2} \in [9.25] $ and $ -2x\in [-10, -6] $, so adding the intervals we get $ [9, 25] + [-10, -6] = [-1, 19] $.

The analysis of the intervals is sensitive to the function's analytical form, for example $ \phi $ can be rewritten as $ \phi = (x - 1)^{2} - 1 $, whose evaluation in $ [3, 5] $, produces the interval $ [3, 15] $,
this introduces us to problems of underestimation or overestimation.

This can happen if we have multiple occurrences of $ \phi $ considered independent during the calculation.
The overestimation has the following characteristics:

\begin{itemize}

\item It is not common.. For example if $ \phi $ has been defined as $ \phi = x^{2} - 2x $ then there will be no overestimation of the function in $ [3, 5] $;

\item We define an interval evaluation as $ [\underline{a}, \overline{a}] $, while $ \phi_m, \phi_M $ as the minimum and maximum real of the $ \phi $ function on a given interval, then the size of the overestimation is $ Max (\phi_m - \underline{a}, \overline{a} - \phi_M) $. \\
The size of the overestimation decreases with the increase of the size of the interval in input, moreover it is possible to decrease it estimating the derivatives of the function $ \phi $, determining the monotony on the considered interval.

\end{itemize}

An interesting property of arithmetic intervals is that they can be implemented taking into account approximation errors.

Suppose that a calculation includes the number $ 1/3 $, clearly there is no floating point able to represent it.
There are floating point numbers $ f_1 $ and $ f_2 $ such that $ f_1 < 1/3 $ and $ f_2 > 1/3 $ and for each calculation that concerns $ 1/3 $, the computer will represent it as a range $ [f_1 , f_2] $.

\subsection*{The Algorithm}

A key issue in troubleshooting with interval analysis is to determine if a $ P (\ textbf {X}) $ property is true.
This property will be true at point $ \textbf{X}_0 $, if $ \textbf{X}_0 $ is solution of the problem, false otherwise.
An example algorithm where the property to satisfy is to find the solutions of an equation system $ \textbf{F(X)} = {F_1 (\textbf{X} = 0), ..., F_N (\textbf{X} = 0)} $ is as follows:

\begin{itemize}

\item Step 1: the algorithm performs the interval evaluation for each $ F_i $ of a given box, where a box is defined as a set of intervals.
If the resulting interval of every $ F_i $ does not include the $ 0 $, then the algorithm will return that no point in $ \textbf{X} $ matches the equations $ F_i $, otherwise the algorithm will return the interval evaluation of each $ F_i $,
that is a set of small intervals, each of which contains the $ 0 $.
Following the evaluation we then obtain a set of intervals such that if the system of equations admits solutions, these will be included in the set.

\item Step 2: Filtering algorithms, which allow to determine if a property is satisfied for a given box. A filter takes in input a box $ \textbf{B} $ and returns $ \textbf{B} $ or a box $ \textbf{B} \ped{f} $ smaller, included in $ \textbf{B} $ . \\
A simple but efficient filter to solve the equations is the 2B method: \\
assumed that there is a solution ($ i $) for $ f(x) = x^{2} + 3x + 1 = 0 $ in the range $ [-10, 10] $, the evaluation of $ f(x) $ in this interval it's $ [-29.131] $ and therefore we can't decide if this interval includes the solution. \\
You can write $ f $ as $ x^{2} = -3x - 1 $ and eventually get a solution to the equation which is the intersection of the evaluation of the left and right side intervals.
For $ x^{2} $ we get $ [0,100] \cap [-31,29] = [0,29] $, from which we know that $ x $ must stay in $ [- \sqrt{29}, \sqrt{29}] $.

\item Step 3 (optional): the existential operator takes a $ \textbf{B} $ box as input and determines whether a single point in a $ \textbf{B}_u $ box satisfies the $ P $ property and also provides a method to calculate it. A typical existential operator for a system of equations can be derived from the Kantorovitch theorem.

\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Un elemento chiave dell'analisi degli intervalli è il processo di bisezione di un box $ \textbf{B} $. 
In questo processo una variabile $ x_i $ viene scelta in un intervallo $ [a,b] $ e vengono ricavati due nuovi box da $ \textbf{B} $, con intervalli identici per tutte le variabili eccetto per $ x_i $ il cui intervallo sarà $ [a, (a+b)/2], [(a+b)/2, b] $.

L'algoritmo processa una lista $\textbf{L}$ di boxes $ \textbf{B} = {\textbf{B}_1,...,\textbf{B}_N} $ , dove l'$ i $-esimo elemento della lista è denotato come $ \textbf{B}_i $, mentre il numero totale di box ad ogni iterazione è denotato come $ r_k $.
All'inizio c'è un singolo box $ \textbf{B}_1 $ in $ \textbf{L} $ e verranno aggiunti nell'esecuzione dopo l'operazione di bisezione (righe 87-88), se $A(B_{k})$ non è né false né true.

Uno schema dell'algoritmo è definito come segue:

\begin{algorithm}[H]
\caption{ Esempio di algoritmo per il calcolo del processo di bisezione }
\begin{algorithmic}[1]{}
\State $i \gets 1$, $k \gets 1$, $r_k \gets 1$
	\While{$ k \leq r_k $}
		\If{$ A(B_k) = wrong $}
			\State $r_{k + 1} \gets r_k$
			\State $k \gets k + 1$
		\Else
			\If{$ A(B_k) = true $}
				\State store $\textbf{B}$ as solution
			\Else
				\State bisect $ B_k $ into $ B_{k}^{1}, B_{k}^{2} $
				\State store $ B_{k}^{1} $ as $ B_{r_{k + 1}} $ and $B_{k}^{2}$ as $ B_{r_{k + 1}} $
				\State $r_{k + 1} = r_{k + 2}$
				\State $k = k + 1$
			\EndIf
		\EndIf
	\EndWhile
\end{algorithmic}
\end{algorithm}

Questo algoritmo termina quando tutti i box di $\textbf{L}$ sono stati processati, tuttavia può capitare che ad un certo punto non riesca a decidere se una proprietà è true o false, per esempio a causa di errori di approssimazione numerica, è possibile utilizzare un flag che consente di gestire questo caso.

Nel caso peggiore la complessità computazionale dell'algoritmo è esponenziale a causa della bisezione.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
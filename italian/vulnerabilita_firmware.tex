\section{Possibili vulnerabilità nei firmware Arduino}
Il drone acquatico è pilotato a basso livello da una scheda Arduino che ha il compito di pilotare i motori del robot, potenza e direzione, più la gestione di una certa quantità di sensori. 
Chi comanda la scheda Arduino è un dispositivo Android che può sia inviare comandi sia ricevere dati.

\subsection{L'ambiente Arduino}

Arduino è un microcontrollore e ambiente di sviluppo, che può essere utilizzato per controllare dispositivi 
e leggere i dati da molti tipi di sensori. Inoltre è facilmente incorporabile in applicazioni esistenti. 
Data la sua natura open source, l'economicità e la flessibilità, Arduino fornisce un framework semplice per un'eventuale implementazione\cite{doukas2012building}.
I primi utenti hanno contribuito scrivendo la documentazione e distribuendo il software da loro sviluppato sotto licenza open source o rendendolo di pubblico dominio. Ciò ha comportato un grande comunità aperta, che facilita l'apprendimento della piattaforma per i nuovi utenti 
e facilita l'iniziare a sviluppare e creare applicazioni.
Tuttavia, mentre il modello di business di Arduino lo ha reso una scelta popolare tra gli utenti non esperti, 
la sicurezza della piattaforma non ha attirato l'attenzione necessaria.

Recentemente è stato mostrato\cite{paper:securityAnalysisArduinoDevices} che Arduino possiede alcune vulnerabilità relative alla gestione della memoria.
Difatti Arduino non fornisce alcun controllo d'errore o altre misure di sicurezza equivalenti, come ad esempio Adress Space Layout Randomization (ASLR)\cite{snow2013just} o la Canary Stack Protection\cite{sinnadurai2008transparent}.

Il chip principale di cui è composto Arduino è \textit{ATmega32u4} basato su architettura AVR, una variante della architettura Harvard\cite{francillon2008code}.
Questo microcontrollore ha 32 registri e la memoria è divisa in tre tipi: EEPROM (come HDD), SRAM (dinamica a tempo di esecuzione), e Flash (dove è salvato il codice).

La figura~\ref{sram_schema} mostra una vista schematica delle aree di memoria più rilevanti della SRAM e i suoi indirizzi di base (ad es.i registri, heap e stack). 
Considereremo queste aree come obiettivi classici per l'exploit del software e altri tipi di attacchi.

I 32 registri occupano posizioni da R0 (0x00) a R31 (0x1F). 
I registri da R26 a R31 hanno alcune funzionalità aggiuntive che li distinguono dai registri general-porpouse che li precedono. 
Questi registri sono puntatori a 16 bit, usati per l'indirizzamento indiretto dello spazio dati.
Più precisamente, questi registri effettuano l'indirizzamento indiretto a 16 bit come segue: 

X = 0x [R27: R26], Y = 0x [R29: R28], e Z = 0x [R31: R30].

L'area dati contiene variabili globali utilizzate dal programma che non sono inizializzate a zero. 
Il segmento BSS mantiene tutte le variabili globali che sono inizializzate a zero e le stringhe costanti. 
L'AVR Stack Pointer (SP) punta all'inizio dello stack (ovvero l'area dati dello stack della SRAM) in cui si trovano lo stack delle Subroutine e degli Interrupt; 
implementati come due registri a 8 bit (SPH: SPL) nello spazio degli I/O.

Lo \textit{stack}, che cresce dall'alto verso il basso, contiene gli indirizzi di ritorno per le subroutine, salvando
registri e variabili locali, e quando cresce, il processo di deallocalizzazione è automatico. 
Tuttavia, lo \textit{heap}, che cresce dal basso verso l'alto, è gestito dalle funzioni \textbf{malloc()}, \textbf{realloc ()} e \textbf{free ()}. 
È condiviso tra tutte le librerie comuni e moduli caricati dinamicamente in un processo e deve essere deallocato manualmente (usando la \textbf{free()}).

\subsection{Analisi di Sicurezza sull'ambiente}

L'analisi delle vulnerabilità per l'ambiente Arduino non è così semplice come nel caso di Linux, e 
non sono disponibili strumenti automatici per la scansione delle vulnerabilità.
Pertanto, è stata condotta la ricerca delle vulnerabilità eseguendo un'analisi statica della mappa degli indirizzi di memoria,
seguendo un approccio "trial-and-error". Inoltre, a differenza di altre architetture come Intel x86 o ARM, Atmel32u4 non possiede strumenti di debug.
Il compilatore IDE Arduino (ad esempio avr-gcc versione 4.8.1) non esegue alcun controllo di sicurezza durante la compilazione.
Pertanto, quando un programma fuoriesce dalla memoria, non viene emesso un avviso o un errore di violazione del segmento.
Si prova quindi a sfruttare errori di tipo out-of-memory per eseguire degli exploit.
Controlliamo gli errori in due diverse parti della memoria SRAM: l'\textit{heap} e lo \textit{stack}.

\begin{itemize}
\item Nel caso dello heap, si è stati in grado di produrre un Heap Buffer Overflow (vedi~\ref{sram_heap_bof}), 
allocando buffer di memoria consecutivamente nello heap (di cui le corrispondenti variabili sono denominate \textbf{cmd} e \textbf{arg}) 
usando la funzione \textbf{malloc()}. L'overflow si verifica quando si sovrascrive la prima serie di dati (cioè \textbf{cmd}) con dati della seconda
serie (cioè \textbf{arg}). Nella prossima sezione forniamo ulteriori dettagli di questa tecnica di exploit.

\begin{figure}[H]
\begin{center}
\label{sram_heap_bof}
\includegraphics[width=0.8\textwidth]{\imgspath Heap_Overflow_Arduino}
\caption{ATmega32u4 Heap Buffer Overflow}
\end{center}
\end{figure}

\item Per quanto riguarda lo stack, è anche possibile eseguire un attacco di tipo Stack Buffer Overflow.
La mancanza degli strumenti di debug rendono difficile ottenere il controllo del flusso del programma.
Inoltre, a causa della separazione fisica di SRAM (che non è eseguibile) e memoria Flash (dove il codice risiede), 
è impossibile per un avversario eseguire codice shell. 
Pertanto, solo gli attacchi basati sul riutilizzo del codice come Return Oriented Programming
sono fattibili, come dimostrato da Francillon e Castellucia\cite{francillon2008code} e recentemente da Habibi e al.\cite{habibi2015mavr}. 
Inoltre, a causa delle dimensioni ridotte delle memorie interne, è possibile notare come l'heap e lo stack possano collidere quando sono eseguite una grande
quantità di chiamate a subroutine con molte variabili locali.

\begin{figure}[H]
\begin{center}
\label{sram_schema}
\includegraphics[width=0.8\textwidth]{\imgspath Process_Memory_Allocation_Arduino}
\caption{ATmega32u4 schema della memoria SRAM}
\end{center}
\end{figure}

\end{itemize}

\subsection{Esempi di attacco}

Un esempio di attacco che sfrutta l'Heap Buffer Overflow è mostrato nel codice seguente.

\lstinputlisting[language=C, caption={Loop in cui si effettua l'Heap Buffer Overflow}, firstline=56, lastline=114]{\vulninopath mal-iot-sample.ino}

Dato il programma vulnerabile descritto precedentemente, possiamo eseguire un attacco Heap buffer Overflow. 
L'obiettivo dell'attacco è sovrascrivere il comando \textit{ifconfig} con un altro comando.
Per eseguire l'attacco, i seguenti dati malevoli sono stati memorizzati come \textbf{arg}, causando la sovrascrittura del buffer di \textbf{cmd} (vedi
Figura~\ref{sram_heap_bof}):
\begin{itemize}
\item 10 bytes di data (che sovrascrivono arg) +
\item 2 bytes aggiuntivi (che sovrascrivono i metadati dello heap) +
\item il comando malevolo che si desidera eseguire
\end{itemize}

\subsubsection*{Stack Buffer Overflow}
Usando una strategia simile a quella dell'attacco precedente, si può eseguire uno stack buffer overflow nell'ambiente Arduino. 
Per fare ciò, si può sviluppare un programma che per prima cosa legga un input dall'interfaccia seriale nel \textbf{main()}, e quindi
passa questo input ad un altro buffer nella funzione \textbf{f1()}. 
L'input originale può avere una lunghezza massima di 30 byte, ma si è impostato il buffer in \textbf{f1()} in modo che sia lungo solamente 10 byte.
Siccome \textbf{strcpy()} non controlla i limiti della memoria, si verificherà un buffer overflow.

Mentre la mancanza di strumenti di debug per Arduino, impedisce di ottenere la comprensione della gestione della memoria, 
si può notare che il programma si arresta in modo anomalo quando vengono inviati specifici payload.
In particolare, si utilizza un approccio black block basato su \textit{trial and error}. 
Più precisamente, dopo aver eseguito l'attacco di buffer overflow, si osserva che l'indirizzo di ritorno dello stack
è stato sovrascritto.

E' noto che il compilatore Integrato in Arduino IDE (avr-gcc versione 4.8.1) non fornisce contromisure contro vulnerabilità riguardanti la corruzione della memoria. 
Randomizzazione dello spazio degli indirizzi (ASLR)\cite{bhatkar2003address} e Stack Canaries\cite{habibi2015mavr} sono due contromisure comuni 
che offuscano il layout di memoria del codice target e prevengono gli overflow dello stack. 
Perciò l'avversario può inviare con attenzione payload specifici per ottenere il flusso di controllo del programma e 
riutilizzare il codice dalla memoria del programma\cite{francillon2008code}\cite{habibi2015mavr}.

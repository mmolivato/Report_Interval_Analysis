\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\beamer@endinputifotherversion {3.36pt}
\defcounter {refsection}{0}\relax 
\select@language {italian}
\defcounter {refsection}{0}\relax 
\select@language {italian}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {1}{Analisi degli Intervalli}{3}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {1}{1}{Introduzione}{5}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {1}{2}{Valutazione degli Intervalli}{8}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {1}{3}{Esempio}{11}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {2}{Casi di Studio}{20}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{1}{Cinematica}{21}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{2}{Analisi delle Singolarit\IeC {\`a}}{25}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {3}{Fault Tree e Analisi degli Intervalli}{30}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {4}{Arduino: possibili vulnerabilit\IeC {\`a}}{50}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{1}{Specifiche}{51}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {4}{2}{Analisi di sicurezza}{58}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {5}{Analisi del nostro Firmware Arduino}{64}{0}{5}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {5}{1}{Panoramica}{65}{0}{5}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {5}{2}{Dichiarazioni di variabili}{68}{0}{5}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {5}{3}{Controllo sulla dimensione degli indici}{72}{0}{5}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {5}{4}{Controllo di intervalli di valori}{77}{0}{5}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {6}{Bibliografia}{81}{0}{6}

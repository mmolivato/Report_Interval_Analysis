\section{Analisi degli Intervalli} 

\subsection{Introduzione}
\begin{frame}
\frametitle{Introduzione}
L'analisi degli intervalli è un metodo inventato dai matematici negli anni '50-'60, per porre dei limiti sugli errori di arrotondamento e di misurazione nel calcolo matematico e quindi sviluppare metodi numerici che producano risultati affidabili. Con questo metodo un valore viene rappresentato come una serie di possibilità.
\end{frame}

\begin{frame}
\frametitle{Analisi degli Intervalli e Robotica}
È un potente metodo numerico che consente di risolvere un ampio range di problemi nel campo della robotica:

\begin{itemize}
\item Risoluzione di sistemi
\item Ottimizzazione
\item Affidabilità robotica\cite{carreras2001interval}
\item Cinematica\cite{rao1998inverse}
\item Motion planning\cite{piazzi2000global}
\item Calibrazione\cite{globOpt}
\end{itemize}

\end{frame}

\subsection{Valutazione degli Intervalli}
\begin{frame}
\frametitle{Operazioni sugli Intervalli}
Per la valutazione(naturale) degli intervalli valgono le operazioni standard:
\begin{itemize}
\item $[x_{1},\ y_{1}] + [x_{2},\ y_{2}] = [x_{1} + x_{2},\ y_{1} + y_{2}]$
\item $[x_{1},\ y_{1}] - [x_{2},\ y_{2}] = [x_{1} - y_{2},\ y_{1} - x_{2}]$
\item $[x_{1},\ y_{1}] * [x_{2},\ y_{2}] = [x_{1} * x_{2},\ y_{1} * y_{2}]$
\item $[x_{1},\ y_{1}] / [x_{2},\ y_{2}] = [x_{1} / y_{2},\ x_{2} / y_{1}]$
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Valutazione degli intervalli}
Sia $ \phi $ una funzione, lo scopo dell'analisi degli intervalli è quello di determinare il massimo $ \overline{F} $ e il minimo $ \underline{F} $.

L'operazione si definisce \textit{valutazione di un intervallo} di una funzione da cui si ottiene $ [\underline{F},\overline{F}] $.\\
Se \textbf{X} denota un insieme di intervalli e $ X\ped{0} $ un'istanza di $ \textbf{X} $, allora abbiamo:
\begin{gather*}
\underline{F} \leq \phi(X_0) \leq \overline{F}
\end{gather*}
\end{frame}

\subsection{Esempio}
\begin{frame}
\frametitle{Valutazione degli intervalli - Esempio}
Supponiamo di voler valutare la funzione $ \phi = x^{2} - 2x $ nell'intervallo $ [3,5] $.
In questo caso possiamo affermare che $ \forall x \in [3, 5] $, allora $ x^{2} \in [9,25] $ e $ -2x \in [-10,-6] $, quindi sommando gli intervalli otteniamo $ [9, 25] + [-10, -6] = [-1, 19] $.

L'analisi degli intervalli è sensibile alla forma analitica che assume la funzione, ad esempio $\phi $ può essere riscritta come $ \phi = (x - 1)^{2} - 1 $, la cui valutazione in $ [3, 5] $, produce l'intervallo $ [3, 15] $, ciò conduce a problemi di sottostima o sovrastima.
\end{frame}

\begin{frame}
\frametitle{Soluzione di un problema}
Un punto chiave nella risoluzione dei problemi attraverso l'analisi degli intervalli è stabilire se vale una certa proprietà $ P(\textbf{X}) $.
Questa proprietà sarà vera al punto $ \textbf{X}_0 $ , se $ \textbf{X}_0 $ è soluzione del problema, falsa altrimenti.
\end{frame}

\begin{frame}
\frametitle{Soluzione di un problema - Esempio}
Un esempio di proprietà è trovare le soluzioni di un sistema di equazioni $ \textbf{F(X)} = {F_1(\textbf{X} = 0),...,F_N(\textbf{X} = 0)} $.\\
Sia:
\begin{itemize}
\item $ \textbf{F(X)} = {F_1(\textbf{X} = 0),...,F_N(\textbf{X} = 0)} $ un sistema di equazioni;
\item $ \textbf{B} $ un box, definito come insieme di intervalli; 
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Soluzione di un problema - Esempio}
Un algoritmo per il calcolo delle soluzioni di un sistema è il seguente:
\begin{itemize}
\item Passo 1: Dato un box $ \textbf{B} $ l'algoritmo esegue la valutazione degli intervalli per ogni $ F_i $.\\
Dalla valutazione otteniamo quindi un insieme di intervalli tali che se il sistema di equazioni ammette soluzioni, queste saranno incluse nell'insieme.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Soluzione di un problema - Esempio}
\begin{itemize}
\item Passo 2: Algoritmi di filtraggio, che consentono di determinare se una proprietà è soddisfatta per un determinato box.

Un filtro prende in input un box $ \textbf{B} $ e ritorna $ \textbf{B} $ oppure un box $ \textbf{B}\ped{f} $ più piccolo, incluso in $\textbf{B}$.
Un semplice ma efficiente filtro per risolvere le equazioni è il metodo 2B:
assunto che esiste una soluzione ($ i $) per $ f(x) = x^{2} + 3x + 1 = 0 $ nell'intervallo $ [-10, 10] $, la valutazione di $ f(x) $ in tale intervallo è $ [-29,131] $ e quindi non possiamo decidere se questo intervallo include la soluzione.\\
Si può scrivere $ f $ come $ x^{2} = -3x - 1 $ e alla fine ottenere una soluzione dell'equazione che è l'intersezione della valutazione degli intervalli del lato sinistro e lato destro.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Soluzione di un problema - Esempio}
\begin{itemize}
\item Passo 3 (opzionale): l'operatore esistenziale prende in input un box $ \textbf{B} $ e determina se un singolo punto in un box $ \textbf{B}_u $ soddisfa la proprietà $ P $ e inoltre fornisce un metodo per calcolarlo.

Un tipico operatore esistenziale per un sistema di equazioni può essere derivato dal Teorema di Kantorovitch\cite{wiki:kantTheorem}.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Operazione di bisezione}
Un elemento chiave dell'analisi degli intervalli è il processo di bisezione di un box $ \textbf{B} $. 
In questo processo una variabile $ x_i $ viene scelta in un intervallo $ [a,\ b] $ e vengono ricavati due nuovi box da $ \textbf{B} $, con intervalli identici per tutte le variabili eccetto per $ x_i $ il cui intervallo sarà $ [a,\ (a+b)/2],\ [(a+b)/2,\ b] $.
\end{frame}
%
%\begin{frame}
%\frametitle{Algoritmo di bisezione}
%Uno schema dell'algoritmo è definito come segue:
%\begin{algorithm}[H]
%\caption{ Esempio di algoritmo per il calcolo del processo di bisezione }
%\begin{algorithmic}[1]{}
%\State $i \gets 1$, $k \gets 1$, $r_k \gets 1$
%	\While{$ k \leq r_k $}
%		\If{$ A(B_k) = wrong $}
%			\State $r_{k + 1} \gets r_k$
%			\State $k \gets k + 1$
%		\Else
%			\If{$ A(B_k) = true $}
%				\State store $\textbf{B}$ as solution
%			\Else
%				\State bisect $ B_k $ into $ B_{k}^{1}, B_{k}^{2} $
%				\State store $ B_{k}^{1} $ as $ B_{r_{k + 1}} $ and $B_{k}^{2}$ as $ B_{r_{k + 1}} $
%				\State $r_{k + 1} = r_{k + 2}$
%				\State $k = k + 1$
%			\EndIf
%		\EndIf
%	\EndWhile
%\end{algorithmic}
%\end{algorithm}
%\end{frame}

\begin{frame}
\frametitle{Operazione di bisezione}
L'algoritmo termina quando tutti i box di $\textbf{L}$ sono stati processati, tuttavia può capitare che ad un certo punto non riesca a decidere se una proprietà è true o false, per esempio a causa di errori di approssimazione numerica, in questa casistica è possibile utilizzare un flag che ne consenta la gestione.
\end{frame}












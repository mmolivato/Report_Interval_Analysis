\section{Analisi di Intervalli}

L'Analisi di Intervalli\cite{moore1966interval} è un potente metodo numerico che consente di risolvere un ampio range di problemi nel campo della robotica, come ad esempio: l'affidabilità dei robot, localizzazione, navigazione, motion planning, collision detection e calibrazione.
Per quanto concerne il calcolo numerico trova applicazione per problemi di approssimazione numerica, di modellazione e controllo robotico.
Nel caso peggiore la complessità computazionale è esponenziale e la scrittura di un algoritmo efficiente richiede un alto livello di esperienza e abilità.

\subsection{Intervalli Aritmetici}

Lo scopo degli intervalli aritmetici è quello di determinare il massimo $ \overline{F} $ e il minimo $ \underline{F} $ di una funzione $ \phi $.
Questa operazione si definisce \textit{valutazione dell'intervallo} di una funzione e ci porta all'intervallo $ [\underline{F},\overline{F}] $  che varia in funzione delle incognite.

Se \textbf{X} denota un insieme di intervalli per le incognite e $ X\ped{0} $ un'istanza di $ \textbf{X} $, allora abbiamo:
\begin{gather*}
\underline{F} \leq \phi(X_0) \leq \overline{F}
\end{gather*}
 
Una valutazione di un intervallo può essere calcolata in diversi modi, il più semplice, chiamato \textit{valutazione naturale}, consiste nell'utilizzo di una versione a intervalli di tutti gli operatori matematici.

Per esempio, l'addizione di due intervalli $ \textbf{a} = [\underline{a}, \overline{a}], \textbf{b} = [\underline{b}, \overline{b}] $ è definita come 
$ \textbf{a} + \textbf{b} = [\underline{a} + \underline{b},  \overline{a} + \overline{b}] $.
La valutazione degli intervalli aritmetici di una funzione può essere illustrata con il seguente esempio: supponiamo di voler valutare la funzione $ \phi = x^{2} - 2x $ nell'intervallo $ [3,5] $.

In questo caso possiamo affermare che $ \forall x \in [3, 5] $, allora $ x^{2} \in [9,25] $ e $ -2x \in [-10,-6] $, quindi sommando gli intervalli otteniamo $ [9, 25] + [-10, -6] = [-1, 19] $.

L'analisi degli intervalli è sensibile alla forma analitica che assume la funzione, ad esempio $\phi $ può essere riscritta come $ \phi = (x - 1)^{2} - 1 $, la cui valutazione in $ [3, 5] $, produce l'intervallo $ [3, 15] $, 
ciò conduce a problemi di sottostima o sovrastima.

Questo può capitare se abbiamo occorrenze multiple di $ \phi $ considerate indipendenti durante il calcolo.
La sovrastima ha le seguenti caratteristiche:

\begin{itemize}

\item Non è frequente, per esempio se $ \phi $ è stata definita come $ \phi = x^{2} - 2x $ allora non ci sarà alcuna sovrastima della funzione in $ [3, 5] $;

\item Definiamo una valutazione dell'intervallo come $ [\underline{a}, \overline{a}]$, mentre $\phi_m, \phi_M $ come il minimo e il massimo reali della funzione $ \phi $ su un dato intervallo, allora la taglia della sovrastima è $ Max(\phi_m - \underline{a}, \overline{a} - \phi_M) $.\\
La taglia della sovrastima decresce con l'aumentare della dimensione dell'intervallo in input, inoltre è possibile diminuirla stimando le derivate della funzione $ \phi $, determinando la monotonia sull'intervallo considerato.

\end{itemize}

Una proprietà interessante degli intervalli aritmetici è che può essere implementata tenendo conto degli errori di approssimazione.

Supponiamo che un calcolo comprenda il numero $ 1/3 $, chiaramente non esiste un floating point in grado di rappresentarlo.
Esistono numeri floating point $ f_1 $ e $ f_2 $ tali che $ f_1 < 1/3 $ e $ f_2 > 1/3 $ e per ogni calcolo che riguarda $ 1/3 $, il computer lo rappresenterà come un intervallo $ [f_1, f_2] $.

\subsection*{Algoritmo}

Un punto chiave nella risoluzione dei problemi con l'analisi degli intervalli è stabilire se una proprietà $ P(\textbf{X}) $ vale.
Questa proprietà sarà vera al punto $ \textbf{X}_0 $ , se $ \textbf{X}_0 $ è soluzione del problema, falsa altrimenti.
Un algoritmo di esempio dove la proprietà da soddisfare riguarda trovare le soluzioni di un sistema di equazioni $ \textbf{F(X)} = {F_1(\textbf{X} = 0),...,F_N(\textbf{X} = 0)} $ è il seguente:

\begin{itemize}

\item Passo 1: l'algoritmo esegue la valutazione degli intervalli per ogni $ F_i $ di un box dato, dove un box è definiti come un insieme di intervalli. 
Se l'intervallo risultante di ogni $ F_i $ non include lo $ 0 $, allora l'algoritmo ritornerà che nessun punto in $ \textbf{X} $ soddisfa le equazioni $ F_i $, altrimenti l'algoritmo ritornerà la valutazione intervallare di ogni $ F_i $, 
ovvero un insieme di piccoli intervalli, ognuno dei quali contenente lo $ 0 $. 
In seguito alla valutazione otteniamo quindi un insieme di intervalli tali che se il sistema di equazioni ammette soluzioni, queste saranno incluse nell'insieme.

\item Passo 2: Algoritmi di filtraggio, che consentono di determinare se una proprietà è soddisfatta per un determinato box. Un filtro prende in input un box $ \textbf{B} $ e ritorna $ \textbf{B} $ oppure un box $ \textbf{B}\ped{f} $ più piccolo, incluso in $\textbf{B}$.\\
Un semplice ma efficiente filtro per risolvere le equazioni è il metodo 2B:\\
assunto che esiste una soluzione ($ i $) per $ f(x) = x^{2} + 3x + 1 = 0 $ nell'intervallo $ [-10, 10] $, la valutazione di $ f(x) $ in tale intervallo è $ [-29,131] $ e quindi non possiamo decidere se questo intervallo include la soluzione.\\
Si può scrivere $ f $ come $ x^{2} = -3x - 1 $ e alla fine ottenere una soluzione dell'equazione che è l'intersezione della valutazione degli intervalli del lato sinistro e lato destro. 
Per $ x^{2} $ si ottiene $ [0,100] \cap [-31,29] = [0,29] $ , dal quale deduciamo che $ x $ deve stare in $ [-\sqrt{29},\sqrt{29}] $.\\

\item Passo 3 (opzionale): l'operatore esistenziale prende in input un box $ \textbf{B} $ e determina se un singolo punto in un box $ \textbf{B}_u $ soddisfa la proprietà $ P $ e inoltre fornisce un metodo per calcolarlo. Un tipico operatore esistenziale per un sistema di equazioni può essere derivato dal Teorema di Kantorovitch.

\end{itemize}

Un elemento chiave dell'analisi degli intervalli è il processo di bisezione di un box $ \textbf{B} $. 
In questo processo una variabile $ x_i $ viene scelta in un intervallo $ [a,b] $ e vengono ricavati due nuovi box da $ \textbf{B} $, con intervalli identici per tutte le variabili eccetto per $ x_i $ il cui intervallo sarà $ [a, (a+b)/2], [(a+b)/2, b] $.

L'algoritmo processa una lista $\textbf{L}$ di boxes $ \textbf{B} = {\textbf{B}_1,...,\textbf{B}_N} $ , dove l'$ i $-esimo elemento della lista è denotato come $ \textbf{B}_i $, mentre il numero totale di box ad ogni iterazione è denotato come $ r_k $.
All'inizio c'è un singolo box $ \textbf{B}_1 $ in $ \textbf{L} $ e verranno aggiunti nell'esecuzione dopo l'operazione di bisezione (righe 87-88), se $A(B_{k})$ non è né false né true.

Uno schema dell'algoritmo è definito come segue:

\begin{algorithm}[H]
\caption{ Esempio di algoritmo per il calcolo del processo di bisezione }
\begin{algorithmic}[1]{}
\State $i \gets 1$, $k \gets 1$, $r_k \gets 1$
	\While{$ k \leq r_k $}
		\If{$ A(B_k) = wrong $}
			\State $r_{k + 1} \gets r_k$
			\State $k \gets k + 1$
		\Else
			\If{$ A(B_k) = true $}
				\State store $\textbf{B}$ as solution
			\Else
				\State bisect $ B_k $ into $ B_{k}^{1}, B_{k}^{2} $
				\State store $ B_{k}^{1} $ as $ B_{r_{k + 1}} $ and $B_{k}^{2}$ as $ B_{r_{k + 1}} $
				\State $r_{k + 1} = r_{k + 2}$
				\State $k = k + 1$
			\EndIf
		\EndIf
	\EndWhile
\end{algorithmic}
\end{algorithm}

Questo algoritmo termina quando tutti i box di $\textbf{L}$ sono stati processati, tuttavia può capitare che ad un certo punto non riesca a decidere se una proprietà è true o false, per esempio a causa di errori di approssimazione numerica, è possibile utilizzare un flag che consente di gestire questo caso.

Nel caso peggiore la complessità computazionale dell'algoritmo è esponenziale a causa della bisezione.
































































\section{Fault Trees}

La crescita tecnologica in ambito robotico ha comportato la necessità di utilizzare robot in ambienti remoti e rischiosi (ambito spaziale, nucleare, marino), dove un errore può avere conseguenze disastrose. Questo ha reso la stima dell'affidabilità uno degli aspetti critici in molte applicazioni robotiche, ad esempio per determinare fattori importanti come:

\begin{itemize}
\item per quanto tempo è in grado di operare una determinata componente
\item ogni quanto eseguire la manutenzione
\item se un sistema è adeguato per certe operazioni.
\end{itemize}


Fault Tree\cite{dugan1996new},\cite{vesely1981fault},\cite{visinsky1994robotic} è un tool per l'analisi della stabilità molto utilizzato dalle industrie nucleari e aerospaziali, tuttavia in quegli ambiti, per la stima dell'affidabilità, hanno a disposizione un set di dati di input accurati. Nel caso della robotica i dati di input sono scarsamente conosciuti, tipicamente incerti e ciò comporta un'incertezza nella stima dell'affidabilità, a causa della mancanza di disponibilità di buone statistiche riguardanti alcuni dispositivi.
Inoltre per robot operanti in ambienti remoti e pericolosi le statistiche raccolte da casi precedenti (tipicamente in ambienti benigni), non sono applicabili direttamente al robot in oggetto.

Tra i possibili approcci alla stima dell'affidabilità in ambito della robotica vi è quello dell'utilizzo dei Fault Trees associati all'Analisi degli Intervalli per trattare problemi di manipolazione di dati incerti ed errori di approssimazione numerica.

Questo approccio codifica il set di dati in termini di intervalli, propagati poi nel Fault Tree per generare un output distribuito che riflette l'incertezza dei dati in input.

La combinazione dei Fault Trees e l'Analisi degli Intervalli apporta dei significativi miglioramenti nell'accuratezza della stima dell'affidabilità rispetto agli 
altri approcci Fault Trees utilizzanti ma che trattano la problematica dell'incertezza con metodi differenti: Dempster-Shafer theory\cite{guth1991probabilistic}, Fuzzy Logic\cite{roberts1996use}, Monte Carlo simulation\cite{web:montecarlo}.

I Robots, in particolar modo i manipolatori robotici, sono dei sistemi elettromeccanici complessi, soggetti a numerosi guasti, dai sensori, attuatori, sistema di guida, all'errore causato da un operatore umano. Questi possono essere causa diretta o combinata ad altri guasti, nel causare il fallimento del sistema.

I Fault Trees forniscono un metodo per analizzare queste complesse interazioni. La rappresentazione avviene mediante struttura ad albero, dove gli eventi base, ovvero le foglie, rappresentano il guasto o fallimento di un sottosistema o componente. Gli eventi base sono combinati mediante porte logiche (AND o OR) in accordo  con la struttura del sistema.

Il risultato finale è dato dalla descrizione del flusso logico degli eventi di guasto o fallimento che si propagano dalle foglie alla radice dell'albero, che rappresenta il fallimento o guasto del sistema complessivo.

Come esempio consideriamo un braccio robotico con tre articolazioni (joint) che opera in un piano.
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.95\textwidth]{\imgspath Robotic_arm}
\caption{Schema di un braccio robotico composto da giunti, attuatori e sensori}
\end{center}
\end{figure}

Il braccio possiede un attuatore e due sensori ridondanti (due sensori di posizione o un sensore di posizione e un sensore di velocità) ad ogni joint, che consentono di avere un certo grado di tolleranza al fallimento.

Dallo scenario appena descritto si può ricavare il seguente Fault Tree:
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.95\textwidth]{\imgspath Fault_tree}
\caption{Rappresentazione tramite porte logiche di un Fault Tree}
\end{center}
\end{figure}

\textbf{Notazioni:}
\begin{itemize}

\item $Ji$ indica l'i-esimo joint con probabilità di guasto $ji$;
\item $Mi$ indica l'i-esimo attuatore con probabilità di guasto $mi$;
\item $SiA$ e $SiB$, il sensore primario (A) e di backup (B) del joint i-esimo con probabilità di guasto $si$;
\item $p$ probabilità di guasto di un componente;
\item $u, v, w, x$ Interval points della probabilità di guasto di un componente. 

\end{itemize}

La principale caratteristica di un Fault Tree dal punto di vista dell'affidabilità è di essere un metodo diretto per il calcolo della probabilità che un particolare evento $s$ (radice dell'albero) si avveri. Dato $s$, l'affidabilità $R$ di un robot è espressa dall'equazione:
\begin{gather*}
R = 1 - s
\end{gather*}
Per il Fault Tree rappresentato in figura, un algoritmo per calcolare la probabilità di guasto alla radice dell'albero ($s$), dagli inputs ($a, b, c, d, e, f, g, h, i$) è dato da:

\begin{itemize}

\item $j = a$ x $b$
\item $k = c$ x $d$
\item $l = e$ x $f$
\item $m = g + j$
\item $n = h + k$
\item $o = i + l$
\item $p = m$ x $n$
\item $q = m$ x $o$
\item $r = n$ x $o$
\item $t = p + q$
\item $s = r + t$

\end{itemize}

Dove "$a$","$b$" sono le probabilità di guasto del sensore primario e ridondante del joint 1 e "$c$","$d$" ed "$e$","$f$" sono le probabilità di guasto dei sensori primari e ridondanti dei joint 2 e 3. Rispettivamente $g, h, i$ sono le probabilità di guasto associate agli attuatori dei joints 1, 2 e 3. Quindi un evento di guasto al braccio robotico (fallimento che si propaga fino alla radice) si verifica se almeno due joint falliscono.

I dati di input sono espressi in termini di intervalli estesi con una "massa", potendo così rappresentare una "densità" nella forma di istogrammi.

Dagli istogrammi si calcola la densità di probabilità di un evento di interesse, come il fallimento del sistema (nodo radice).

La rappresentazione mediante istogrammi fa utilizzo di alcuni modelli predefiniti chiamati "\emph{griglie}" (grids), che consentono di controllare il tradeoff tra l'accuratezza del risultato e la complessità di calcolo.
Uno dei principali vantaggi di questo metodo è quello di poter fornire una simulazione esaustiva dello spazio di input in tempo ragionevole, inoltre vi è la garanzia che i risultati ottenuti dalla simulazione del caso pessimo siano inclusi negli istogrammi in output. Vantaggio che altri metodi basati sulla simulazione parziale dello spazio di input (es. Monte Carlo) non possono garantire, tali metodi riescono a rappresentare il caso peggiore solamente con una simulazione che prevede ogni possibile esecuzione, quindi maggiore tempo di computazione.

Prendiamo in esempio il caso di un attuatore (motore elettrico), una distribuzione di input con gli istogrammi è costruita come segue:

\begin{itemize}

\item $m = (0, 0.000739) : 0.05$
\item $m = (0.000739, 0.00203) : 0.11$
\item $m = (0.00203, 0.00924) : 0.34$
\item $m = (0.00924, 0.04158) : 0.34$
\item $m = (0.04158, 0.10903) : 0.11$
\item $m = (0.10903, 1) : 0.05$

\end{itemize}

\begin{figure}[H]
\begin{center}
\includegraphics[width=0.8\textwidth]{\imgspath Istogramma_1}
\caption{Probabilità di fallimento di un motore}
\end{center}
\end{figure}

Così la funzione $m$, rappresentante la probabilità di fallimento di un attuatore $Mi$, ha lo 0.05 della sua massa di probabilità tra lo 0 e 0.000739, ecc.

\subsection{Definizione e regole}

Gli istogrammi sono descritti in termini di intervalli che sono definiti come

\begin{mydef}
Un intervallo $[a, b]/p$ è un insieme $N = (b - a + 1)$ di interi $x$ che verifica $a \leq x \leq b$ con una massa di probabilità $p$.
$p$ è uniformemente distribuito in $[a, b]$ quindi la probabilità per ogni $x \in [a, b]$ può essere calcolata come $p/N$.
\end{mydef}

La definizione di intervallo appena fornita è basata sui numeri interi, quindi gli intervalli vengono preprocessati (postprocessati) per poter essere applicati nei problemi di affidabilità dove i dati si basano su numeri razionali.

In particolare, l'intervallo $[0, 1]$ viene partizionato in $M$ passi di discretizzazione di uguale misura rappresentati da un numero (\emph{midpoint}) e una probabilità (ottenuta dalla distribuzione originale). Così l'intervallo viene scalato nell'intervallo $[0, M - 1]$ e mappato in una distribuzione di interi nello stesso intervallo, dove ogni intero $n$ ha la probabilità del sottointervallo $[n - 0.5, n + 0.5]$ nella distribuzione scalata. Si noti che scalare può richiedere alcune operazioni addizionali (moltiplicazione e divisione per $M$), durante il calcolo del Fault Tree, questo per mantenere il comportamento originale, inoltre il valore di $M$ assume un ruolo importante nella computazione, in quanto determina il grado di accuratezza numerica.

In generale, $M$ è selezionato in accordo con la precisione fornita nei dati in input, per esempio, se un input possiede $m$ cifre, allora $M=10^{m}$.

La principale caratteristica di questo approccio è di essere basato su dei modelli chiamati "\emph{griglie}" (grids) per rappresentare istogrammi.

Le griglie stabiliscono la granularità dei dati, fornendo un metodo per controllare il tradeoff tra accuratezza dei dati e tempo di calcolo.

In generale la complessità di calcolo dei metodi basati sugli intervalli è data dalla taglia dello spazio di input e dal processo di raccolta dei risultati, per ogni esecuzione, in un istogramma di output.

Se si utilizza un modello a intervalli senza limiti, nella fase di raccolta dei dati il numero delle barre dell'istogramma di output tenderà a crescere (Unendo due intervalli la cui intersezione è diversa dal vuoto, produrrà tre intervalli in output), con l'aumento dei tempi di raccolta che diverranno proibitivi.

Tale problema giustifica l'utilizzo delle griglie, che si possono definire come una lista di intervalli disgiunti e adiacenti che coprono l'intero intervallo numerico di interesse.

Si possono definire griglie sia per controllare la taglia dello spazio di input, sia per il processo di raccolta dei dati e sono caratterizzate da quattro parametri:
\begin{itemize}

\item il \textbf{tipo}: lineare (formato da intervalli di taglia uguale) o geometrico (formato da intervalli la cui taglia cresce esponenzialmente a partire dal centro di simmetria);
\item la \textbf{granularità} ($g$): rappresenta la taglia dei suoi intervalli;
\item il \textbf{centro di simmetria} ($c$): la griglia sarà simmetrica rispetto al suo centro di simmetria;
\item il \textbf{focus} ($f$): per ridurre la complessità di calcolo limitando la regione di spazio numerico che è analizzata in dettaglio.

\end{itemize}

\begin{mydef}[\textbf{Griglia lineare}]
E' un insieme di intervalli adiacenti $[A, B]$, ognuno dei quali unicamente identificato da un intero $n$, che verifica una delle seguenti identità:
\begin{gather*}
[A, B] = \left\{
\begin{aligned}
& [-Inf, c + g(-f + 1)] & (n < -f) \\
& [c + gn + 1, c + g(n + 1)] & (-f < n < -1) \\
& [c - g + 1, c - 1] & (n = -1) \\
& [c, c] & (n = 0) \\
& [c + 1, c + g - 1] & (n = 1)  \\
& [c + g(n - 1), c + gn - 1] & (f > n > 1) \\
& [c + g(f - 1), Inf] & (n \geq f) \\
\end{aligned}
\right.
\end{gather*}

\end{mydef}

\begin{mydef}[\textbf{Griglia geometrica}]
E' un insieme di intervalli adiacenti $[A, B]$, ognuno dei quali unicamente identificato da un intero $n$, che verifica una delle seguenti identità:
\begin{gather*}
[A, B] = \left\{
\begin{aligned}
& [-Inf, c - g^{f - 1}] & (n < -f) \\
& [c - g^{-n} + 1, c - g^{-(n + 1)}] & (-f < n < 0) \\
& [c, c] & (n = 0) \\
& [c + g^{n - 1} + 1, c + g^{n} - 1] & (f > n > 0) \\
& [c + g^{f - 1}, Inf] & (n \geq f) \\
\end{aligned}
\right.
\end{gather*}

Con g, f interi positivi.
\end{mydef}

\subsection{Algoritmo di computazione}

Una volta che la distribuzione in input è stata rappresentata in termini di una griglia predefinita, il metodo di computazione esegue:

\begin{itemize}

\item Considerato lo spazio di input $N1 \times N2 \times ... \times NI$, dove $Ni$ è l'insieme di intervalli che descrive l'istogramma dell'input $i$;

\item Per ogni vettore $(..., [a_{ij}, b_{ij}]/p_{ij}, ...)$ dello spazio di input, dove $[a_{ij}, b_{ij}]/p_{ij}$ rappresenta la j-esima barra dell'istogramma che descrive l'input $i$:
	
\begin{enumerate}[i.]

\item Calcola la sua probabilità $ P = \prod_{i = 1}^{I}p_{ij} $;
\item Esegue le operazioni usando gli intervalli aritmetici;
\item Assegna $P$ ad ogni intervallo risultante;
\item Raccoglie i risultati in istogrammi di output, applicando le regole di split e merge.

\end{enumerate}

\end{itemize}
\begin{mydef}[\textbf{Regola di split}] Ogni intervallo che risulta estendersi su diversi intervalli della griglia, viene partizionato in altrettanti intervalli con probabilità proporzionali.
\end{mydef}

\begin{mydef}[\textbf{Regola di merge}] Tutti i risultati dell'intervallo che risultano essere nello stesso intervallo della griglia, vengono rappresentati come un singolo intervallo con probabilità sommate.
\end{mydef}
 
Dal momento che l'approccio a intervalli calcola il Fault Tree per ogni possibile combinazione di intervalli di input, produce una stima completa della distribuzione in uscita, non aggiungendo complessità di calcolo significativa. L'overhead computazionale causato dalla fase di raccolta dei risultati può essere comparato all'overhead causato dalla generazione random dei sample con il metodo Monte Carlo, eccetto quando viene utilizzato una griglia molto dettagliata per l'output, in questo caso ogni intervallo eseguito sul Fault Tree impiega il doppio del tempo di Monte Carlo.

L'utilizzo dell'approccio a intervalli è particolarmente interessante per calcolare grandi spazi di input dominati da un grande numero di intervalli di input, tuttavia è meno efficace quando lo spazio di input è dominato da grandi numeri (medesima problematica per il metodo Monte Carlo).

Alcuni studi\cite{web:montecarlo} suggeriscono l'utilizzo di euristiche, altri\cite{paper:bid} la partizione del Fault Tree in alberi di dimensioni ridotte indipendenti (semi-indipendenti) che verranno calcolati separatamente.